import { InMemoryDbService } from 'angular-in-memory-web-api';


export class InMemoryDataService implements InMemoryDbService {

createDb(){
const heroes =[
{ id: 11, name: 'Mr. Nice',age:32 },
  { id: 12, name: 'Narco', age:132 },
  { id: 13, name: 'Bombasto', age:55 },
  { id: 14, name: 'Celeritas' , age:55},
  { id: 15, name: 'Magneta' , age:55},
  { id: 16, name: 'RubberMan' , age:55},
  { id: 17, name: 'Dynama' , age:55},
  { id: 18, name: 'Dr IQ' , age:55},
  { id: 19, name: 'Magma' , age:55},
  { id: 20, name: 'Tornado', age:55 }];

  return{heroes};
  }
}