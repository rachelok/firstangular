import { Component, OnInit,Input } from '@angular/core';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HeroService }  from '../hero.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
  private heroService: HeroService,
  private location: Location) { }

  @Input() myhero:Hero;

  ngOnInit() {
   this.getHero();
  }

  getHero():void{
  	const id = +this.route.snapshot.paramMap.get('id');
  	this.heroService.getHero(id)
  		.subscribe(myhero=> this.myhero = myhero);
  }
  save(): void {
   this.heroService.updateHero(this.myhero)
     .subscribe(() => this.goBack());
 }
  goBack(): void {
  this.location.back();
  }
}
