import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyheroComponent } from './myhero/myhero.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes =[
	{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
	{path:'myhero', component : MyheroComponent },
	{path:'dashboard',component:DashboardComponent},
	{path:'details/:id',component:DetailsComponent}
];

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }