import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-myhero',
  templateUrl: './myhero.component.html',
  styleUrls: ['./myhero.component.css']
})
export class MyheroComponent implements OnInit {

  
  myheroes : Hero[];
  selectedHero : Hero;

  
  constructor(private heroService:HeroService){ }

  onSelect(hh:Hero):void{
  this.selectedHero=hh;
  }

  getHeroes() : void{
  	this.heroService.getHeroes()
  		.subscribe(myheroes => this.myheroes = myheroes);
  }
  
 
  ngOnInit() {
  	this.getHeroes();
  }

  add(name: string, age:number): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name,age } as Hero)
      .subscribe(hero => {
        this.myheroes.push(hero);
      });
  }

  delete(hero: Hero): void {
  	this.myheroes = this.myheroes.filter(h => h !== hero);
  	this.heroService.deleteHero(hero).subscribe(); 
  }



}
