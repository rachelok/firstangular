import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  myheroes : Hero[];

  constructor(private heroService:HeroService) { }

  ngOnInit() {
  	this.getHeroes();
  }

  getHeroes() : void{
  	this.heroService.getHeroes()
  		.subscribe(myheroes => this.myheroes = myheroes.slice(1,5));
  }

}
